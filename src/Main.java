import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws Exception{
        Resto uus = new Resto("McDonalds", "https://....", "Uus 111", 20, "burks", 2, 1);

        ArrayList restod = Tegevused.nimistuSaaja();

        int suvaNum = Tegevused.suvalineArv(restod);
        System.out.println(suvaNum);

        Resto resto = Tegevused.restoValija(restod, suvaNum);
        System.out.println(resto.nimi);

        ArrayList restoArrayList = Tegevused.objektArLiks(uus);
        System.out.println(restoArrayList);

        // Tegevused.restoLisaja(restoArrayList);

        Resto maschu = new Resto("Maschu maschu", "https://...", "Wien 1030", 300, "vege", 10, 4);
        ArrayList maschuArLi = Tegevused.objektArLiks((maschu));
        //Tegevused.restoLisaja(maschuArLi);


        //muudame maschu kodulehte
        System.out.println(maschu.koduleht);
        maschu.koduleht = "https://maschu.ee";
        System.out.println(maschu.koduleht);

        ArrayList maschuMa = Tegevused.objektArLiks(maschu);
        System.out.println(maschuMa);
        Tegevused.restoMuutja(maschuMa);

    }
}
