import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Tegevused {

    // laeb sisse restoranide nimistu (txt faili) ArrayListi
    public static ArrayList nimistuSaaja() throws Exception {
        ArrayList restod = new ArrayList();
        File file = new File("restod.txt");
        Scanner restodFailist = new Scanner(file);
        while(restodFailist.hasNextLine()){
            String line = restodFailist.nextLine();
            restod.add(line);
        }
        return restod;
    }

    /*
    loeb kokku, mitu elementi restode ArrayListis on (mitu restorani on)
    tagastab suvalise arvu vahemikus 0 kuni (restode arv - 1), et saaks suvaliselt resto valida
     */
    public static int suvalineArv(ArrayList restod) {
        int num = restod.size();
        int randInt = (int)(Math.random() * num);
        return randInt;
    }

    //tagastab suvalise arvu põhjal restode ArrayListist ühe restorani objekti kujul
    public static Resto restoValija(ArrayList restod, int randInt){
        String yksResto = (String) restod.get(randInt);
        String[] yksRestoArr = yksResto.split(";");
        // String nimi, String koduleht, String aadress, int kaugusMinutites, String toidutyyp, int hinnangToit, int ooteaeg
        String nimi = yksRestoArr[0];
        String koduleht = yksRestoArr[1];
        String aadress = yksRestoArr[2];
        int kaugusMinutites = Integer.parseInt(yksRestoArr[3]);
        String toidutyyp = yksRestoArr[4];
        int hinnangToit = Integer.parseInt(yksRestoArr[5]);
        int ooteaeg = Integer.parseInt(yksRestoArr[6]);

        Resto resto = new Resto(nimi, koduleht, aadress, kaugusMinutites, toidutyyp, hinnangToit, ooteaeg);
        return resto;
    }

    public static ArrayList objektArLiks (Resto resto){
        ArrayList restoArList = new ArrayList();
        restoArList.add(resto.nimi);
        restoArList.add(resto.koduleht);
        restoArList.add(resto.aadress);
        restoArList.add(resto.kaugusMinutites);
        restoArList.add(resto.toidutyyp);
        restoArList.add(resto.hinnangToit);
        restoArList.add(resto.ooteaeg);
        return restoArList;
    }

    //kirjutab restode txt faili ühe resto juurde
    public static void restoLisaja(ArrayList resto){
        String restoStr = "";
        restoStr += resto.get(0);
        for (int i = 1; i < resto.size(); i++) {
            restoStr += ";" + resto.get(i);
        }
        try
        {
            FileWriter fw = new FileWriter("restod.txt",true); //the true will append the new data
            fw.write((("\n")+restoStr));//appends the string to the file
            fw.close();
        }
        catch(IOException ioe)
        {
            System.err.println("IOException: " + ioe.getMessage());
        }

    }
    // uuendab sisendi (hinnangu andmise) korral restorani andmeid
    public static void restoMuutja(ArrayList resto) throws Exception {

        //muudab ArrayListi stringiks
        String restoStr = "";
        restoStr += resto.get(0);
        for (int i = 1; i < resto.size(); i++) {
            restoStr += ";" + resto.get(i);
        }

        //tuvastab, mitmendat rida muuta tuleks
            //loeb txt faili sisse ArrayListiks
        ArrayList restod = Tegevused.nimistuSaaja();
        System.out.println(restod);
            //otsib nime põhjal välja õige resto rea ja asendab selle muudetud vormiga
        for (int i = 0; i < restod.size(); i++) {
            String yksResto = (String) restod.get(i);
            String[] yksRestoArr = yksResto.split(";");
            if(yksRestoArr[0].equals(resto.get(0).toString())) {
                restod.set(i, restoStr);
            }
        }

        FileWriter fw = new FileWriter("restod.txt", false);
        fw.write(""); //teeb faili tühjaks
        fw.close();

        FileWriter fw2 = new FileWriter("restod.txt", true);
        for (int i = 0; i < restod.size(); i++) {
            fw2.write((String) restod.get(i) + ("\n"));
        }
        fw2.close();
    }

}
