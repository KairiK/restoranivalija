import java.util.ArrayList;

public class Resto {
    String nimi;
    String koduleht;
    String aadress;
    int kaugusMinutites;
    String toidutyyp;
    int hinnangToit;
    int ooteaeg;


    public Resto(String nimi, String koduleht, String aadress, int kaugusMinutites, String toidutyyp, int hinnangToit, int ooteaeg){
        this.nimi = nimi;
        this.koduleht = koduleht;
        this.aadress = aadress;
        this.kaugusMinutites = kaugusMinutites;
        this.toidutyyp = toidutyyp;
        this.hinnangToit = hinnangToit;
        this.ooteaeg = ooteaeg;
    }
}
