

package com.example.demo;

import ee.restovalija.Main;
import ee.restovalija.Resto;
import ee.restovalija.Tegevused;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;

@CrossOrigin
@RestController
public class APIController {

    public APIController() {
    }

    @RequestMapping("/index")
    String index() {
        String nimi = Main.getRandomResto();
        return nimi;
    }

    @PostMapping("/uusresto")
    String uusResto(@RequestBody Resto resto) {
        String sone = resto.getNimi();
        System.out.println(sone);

        ArrayList restoList = Tegevused.objektArLiks(resto);
        Tegevused.restoLisaja(restoList);
        return "Tere";
    }

    @PostMapping("/hindaresto")
    String hindaResto(@RequestBody Resto resto) throws Exception {
        ArrayList kolmAtr = Tegevused.objektArLiks(resto);
        String nimi = kolmAtr.get(0).toString();
        int hinnangToit = (int) kolmAtr.get(5);
        int ooteaeg = (int) kolmAtr.get(6);
        ArrayList uuendatud = Tegevused.uuendatudList(nimi, hinnangToit, ooteaeg);
        Tegevused.restoMuutja(uuendatud);
        System.out.println(resto.getNimi());
        return "Tere";
    }
    /*
    @RequestMapping("/tervislik")
    String tervislik() {
        return "Miku Vegan";
    }

    @GetMapping("/ll/{liha}")
    String messages(@PathVariable String liha) {
        return "Sinu lihavalik on: " + liha;
    }
    */

    /*
    @GetMapping("/chat/{room}")
    ChatRoom messages(@PathVariable String room) {
        return chatrooms.get(room);
    }

    @PostMapping("/chat/{room}/new-message")
    ChatMessage newMessage(@RequestBody ChatMessage message, @PathVariable String room) {
        ChatRoom chatroom = chatrooms.get(room);
        chatroom.addMessage(message);
        System.out.println(message);
        return message;
    }
*/

}
