package ee.restovalija;

import java.util.ArrayList;

public class Resto {
    private String nimi;
    private String koduleht;
    private String aadress;
    private int kaugusMinutites;
    private String toidutyyp;
    private int hinnangToit;
    private int ooteaeg;


    public Resto(String nimi, String koduleht, String aadress, int kaugusMinutites, String toidutyyp, int hinnangToit, int ooteaeg){
        this.nimi = nimi;
        this.koduleht = koduleht;
        this.aadress = aadress;
        this.kaugusMinutites = kaugusMinutites;
        this.toidutyyp = toidutyyp;
        this.hinnangToit = hinnangToit;
        this.ooteaeg = ooteaeg;
    }

    public Resto(){}

    public String getNimi() {
        return nimi;
    }

    public String getKoduleht() {
        return koduleht;
    }

    public String getAadress() {
        return aadress;
    }

    public int getKaugusMinutites() {
        return kaugusMinutites;
    }

    public String getToidutyyp() {
        return toidutyyp;
    }

    public int getHinnangToit() {
        return hinnangToit;
    }

    public int getOoteaeg() {
        return ooteaeg;
    }
}
