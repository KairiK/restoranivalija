#!/bin/sh

touch ~/pid.file

kill $(cat ~/pid.file)

nohup java -jar build.jar > log.txt 2> errors.txt < /dev/null &

echo $! > ~/pid.file